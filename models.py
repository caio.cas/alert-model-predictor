import json
import pydantic
from pydantic import BaseModel
from typing import Optional, List
from bentoml.io import JSON
from bentoml.exceptions import BadInput


class ClimateFactor(BaseModel):
    factor_name: str
    score: float
    date: str
    company_average: float


class Evaluation(BaseModel):
    score: float
    score_description: str
    date: str


class ExtraHours(BaseModel):
    date: str
    hours: float


class Person(BaseModel):
    id: int
    date_of_hire: str
    company_time_in_days: int
    spam_of_control: int
    manager_climate_factors: List[ClimateFactor]
    number_of_raises_without_adjusts: float
    evaluation: Optional[Evaluation]
    last_six_months_extra_hours: List[ExtraHours]
    last_raise_date_without_adjusts: Optional[str]


class Alerts(BaseModel):
    id: int
    extra_hour_alert: str
    raise_alert: str
    turnover_alert: str
    spam_of_control_alert: str


class ListJSON(JSON):
    """Deals with list of json objects in input"""

    async def from_http_request(self, request):
        json_str = await request.body()
        try:
            json_obj = json.loads(json_str)
        except json.JSONDecodeError as e:
            raise BadInput(f"Invalid JSON input received: {e}") from None

        if self._pydantic_model:
            if isinstance(json_obj, list):
                obj_list = json_obj
            else:
                obj_list = [json_obj]

            try:
                pydantic_model_list = [
                    self._pydantic_model.parse_obj(obj) for obj in obj_list
                ]
                return pydantic_model_list
            except pydantic.ValidationError as e:
                raise BadInput(f"Invalid JSON input received: {e}") from None
        else:
            return json_obj
