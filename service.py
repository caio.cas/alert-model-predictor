import bentoml
import numpy as np
from datetime import datetime, timedelta
from models import ExtraHours, ListJSON, Person, Alerts
from typing import List, Dict
alert_predictor_runner = bentoml.sklearn.get(
    "alert_predictor:latest").to_runner()

svc = bentoml.Service("alert_predictor", runners=[alert_predictor_runner])


def group_extra_hours_by_month(
    last_six_months_extra_hours: List[ExtraHours],
) -> Dict[str, float]:
    months_group = {}

    for extra_hour in last_six_months_extra_hours:
        year_month = extra_hour.date.split(
            "-")[0] + "-" + extra_hour.date.split("-")[1]

        if year_month not in months_group:
            months_group[year_month] = 0

        months_group[year_month] += extra_hour.hours

    return months_group


def calculate_extra_hour_average_last_four_months(
    months_group: Dict[str, float]
) -> List[float]:
    now = datetime.now()

    last_four_months_average = []

    for i in range(1, 5):
        month = now.month - i
        year = now.year

        if month < 1:
            month += 12
            year -= 1

        if f"{year}-{month:02d}" in months_group:
            last_four_months_average.append(
                months_group[f"{year}-{month:02d}"])
        else:
            last_four_months_average.append(0)

    return last_four_months_average


def extra_hour_alert(person: Person) -> str:
    """
    Calcular a média e desvio padrão dos últimos 4 meses.

    IF(
        AND(
            média horas extras <= 5;
            desvio horas extras <= 5
        );
        "Sem alerta";
        IF(
            AND(média horas extras <= 10;desvio horas extras <= 5);
            "Constante necessidade de horas extras";
            IF(
                AND(
                    média horas extras <= 10;
                    desvio horas extras >5
                );
                "Alta carga periódica de horas extras";
                "Constante alta carga de horas extras"
            )
        )
    )

    """
    average_extra_hours_last_four_months = (
        calculate_extra_hour_average_last_four_months(
            group_extra_hours_by_month(person.last_six_months_extra_hours)
        )
    )

    average_extra_hours = np.average(average_extra_hours_last_four_months)

    standard_deviation_extra_hours = np.std(
        average_extra_hours_last_four_months)

    if average_extra_hours <= 5 and standard_deviation_extra_hours <= 5:
        return "Sem alerta"
    elif average_extra_hours <= 10 and standard_deviation_extra_hours <= 5:
        return "Constante necessidade de horas extras"
    elif average_extra_hours <= 10 and standard_deviation_extra_hours > 5:
        return "Alta carga periódica de horas extras"
    else:
        return "Constante alta carga de horas extras"


def raise_alert(person: Person) -> str:
    """
    Primeiro, dividir o número de anos que a pessoa tem na empresa pela quantidade de aumentos por mérito ao aumento.

    IF(
        AND(
            média de aumentos por mérito<=0.5;
            última AVD = "Outstanding Performance";
            data do último aumento < (HOJE()-1095)
        );
        "Crítico";
            IF(
                AND(
                    média de aumentos pro mérito <=0.5;
                    OU(
                        AJ2="Strong Performance";
                        AJ2="Outstanding Performance"
                    );
                    data último aumento < (HOJE()-720);
                    Q2<(HOJE()-720)
                );
                "Moderado";
                Sem alerta
            )
        )
    """
    if not person.last_raise_date_without_adjusts:
        last_raise_date = datetime.min
    else:
        last_raise_date = datetime.strptime(
            person.last_raise_date_without_adjusts, "%Y-%m-%d"
        )

    raises_average = person.number_of_raises_without_adjusts / (
        person.company_time_in_days / 365
    )

    if person.evaluation is None:
        return "Sem alerta"

    if (
        raises_average <= 0.5
        and person.evaluation.score_description == "Outstanding Performance"
        and last_raise_date < (datetime.now() - timedelta(days=1095))
    ):
        return "Crítico"

    elif (
        raises_average <= 0.5
        and (
            person.evaluation.score_description == "Strong Performance"
            or person.evaluation.score_description == "Outstanding Performance"
        )
        and last_raise_date < (datetime.now() - timedelta(days=720))
    ):
        return "Moderado"

    else:
        return "Sem alerta"


def turnover_alert(person: Person, raise_alert_result: str) -> str:
    """
    IF(
        AND(
            OR(
                Delta clima fator 2 - company_average <0;
                Delta clima fator 3 - company_average <0
            );
            quantidade de aumentos por mérito = 0;
            possui qualquer alerta aumento
        );
        "Crítico";
        IF(
            OR(
                Delta clima fator 2 - company_average < 0;
                Delta clima fator 3 - company_average < 0;
            );
            "Moderado";
            "Não crítico"
        )
    )
    """
    if len(person.manager_climate_factors) > 0:
        climate_factor_2 = {}
        climate_factor_3 = {}

        for climate_factor in person.manager_climate_factors:
            if climate_factor.factor_name == "Categoria #2":
                climate_factor_2 = climate_factor

            if climate_factor.factor_name == "Categoria #3":
                climate_factor_3 = climate_factor

        delta_clima_condition = (
            climate_factor_2.score - climate_factor_2.company_average
        ) or (climate_factor_3.score - climate_factor_3.company_average)
    else:
        delta_clima_condition = False

    if (
        delta_clima_condition
        and person.number_of_raises_without_adjusts == 0
        and (raise_alert_result == "Crítico" or raise_alert_result == "Moderado")
    ):
        return "Crítico"

    elif delta_clima_condition:
        return "Moderado"

    return "Sem alerta"


def spam_of_control_alert(person: Person) -> str:
    if person.spam_of_control > 15:
        return "Crítico"
    elif person.spam_of_control > 10:
        return "Moderado"

    return "Sem alerta"


@svc.api(
    input=ListJSON(pydantic_model=Person),
    output=ListJSON(pydantic_model=Alerts),
)
async def classify(input_series: List[Person]):
    results = []
    for person in input_series:
        raise_alert_result = raise_alert(person)
        results.append(
            {
                "id": person.id,
                "extra_hour_alert": extra_hour_alert(person),
                "raise_alert": raise_alert_result,
                "turnover_alert": turnover_alert(person, raise_alert_result),
                "spam_of_control_alert": spam_of_control_alert(person),
            }
        )
    return results
