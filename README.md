# Alert Model Predictor

## Setup

Install poetry and a python 3.10 version

```sh
poetry env use 3.10.6 # Optional
poetry install
poetry shell
```

## Get new model version

```sh
pyton train.py
```

## Serve locally

```sh
bentoml serve service.py:svc --reload
```

## Push new version to yatai

```sh
bentoml build
bentoml push
```

# Build new docker image

```sh
bentoml containerize <MODEL_TAG>
```
